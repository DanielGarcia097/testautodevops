var assert = require('assert')
var pilota = require('../src/ServicioConductor/src/routes/conductor.js')



describe('Servicio Conductor', function () {
  it('should have a Servicio condutor method', function () {
    var piloto = pilota.localizarPiloto('C209ZMK', 'Ciudad de Guatemala')
    var id = piloto.Id
    var nombre = piloto.Nombre
    var vehiculo = piloto.Vehiculo
    var telefono = piloto.Telefono

    assert.strictEqual(id, '2')
    assert.strictEqual(nombre, 'Conductor Juan Perez')
    assert.strictEqual(telefono, '12345678')
    assert.strictEqual(vehiculo, 'C209ZMK')
  })
})
